package octreej;

import org.junit.Assert;
import org.junit.Test;

public class OctreeUtilTest {
  @Test
  public void checks() {
    Assert.assertFalse(OctreeUtil.checkAndWhichHalf(5, 3, 4));
    Assert.assertFalse(OctreeUtil.checkAndWhichHalf(50, 0, 64));
  }
}
