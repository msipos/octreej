package octreej;

import java.util.Iterator;
import org.junit.Test;
import static org.junit.Assert.*;

public class OctreeTest {
  @Test
  public void setAndGet() {
    Octree<Integer> octree = new Octree<Integer>(512);
    octree.set(50, 50, 50, 1);
    assertEquals(octree.get(50, 50, 50), (Integer) 1);
    octree.set(50, 50, 51, 3);
    assertEquals(octree.get(50, 50, 51), (Integer) 3);
    octree.set(50, 50, 50, 2);
    assertEquals(octree.get(50, 50, 50), (Integer) 2);
    octree.set(30, 40, 55, 4);
    assertEquals(octree.get(30, 40, 55), (Integer) 4);
  }

  @Test
  public void iterator() {
    Octree<Integer> octree = new Octree<Integer>(1024);
    int N = 1000;
    for (int i = 0; i < N; i++) {
      octree.set(i, i, i, i);
    }

    int x = 0;
    for (OctreeElement<Integer> i : octree) {
      x++;
    }
    assertEquals(x, N);
  }

  @Test
  public void subiterator() {
    int N = 16*1024;
    Octree<Integer> octree = new Octree<Integer>(N);
    for (int i = 0; i < N; i++) {
      octree.set(i, i, i, i);
    }

    Iterator<Octree<Integer>> iter = octree.getSubIterator(8);
    int y = 0;
    while (iter.hasNext()) {
      int x = 0;

      Octree<Integer> oct = iter.next();
      for (OctreeElement<Integer> i : oct) {
        x++;
      }
      assertEquals(x, 8);
      y += x;
    }
    assertEquals(y, N);
  }
}
