package octreej;

enum IteratorState {
  GOING, FINISHED;
}
