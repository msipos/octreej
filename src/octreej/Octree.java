package octreej;

import java.util.Iterator;

/**
 * Documented, user-friendly, implementation of a growing Octree for Java. This
 * implementation only allows data to be stored at the end-nodes of the Octree.
 * In that sense, the octree can be considered a map of triples of integers to
 * data elements, that is (x, y, z -> element).  Indices x, y and z range from 0
 * to size-1. Size must be at least 4, and it must be a power of 2.
 * 
 * @author Maksim Sipos
 * 
 * @param <E>
 *          Type of data stored in the Octree.
 */
public class Octree<E> implements SubOctree<E>, Iterable<OctreeElement<E>> {
  private final int size;

  /**
   * @return size of each dimension of the Octree.
   */
  public int getSize() {
    return size;
  }

  private final int x1, y1, z1;

  /**
   * Returns the lowest x index within the Octree. The octree contains elements
   * with x indices in the range [x, x+size). This method is only useful when
   * used with the sub-octree iterator since the lowest x index for a whole
   * Octree is always 0.
   */
  public int getX() {
    return x1;
  }

  /**
   * Returns the lowest y index within the Octree. The octree contains elements
   * with y indices in the range [y, y+size). This method is only useful when
   * used with the sub-octree iterator since the lowest y index for a whole
   * Octree is always 0.
   */
  public int getY() {
    return y1;
  }

  /**
   * Returns the lowest z index within the Octree. The octree contains elements
   * with z indices in the range [z, z+size). This method is only useful when
   * used with the sub-octree iterator since the lowest z index for a whole
   * Octree is always 0.
   */
  public int getZ() {
    return z1;
  }

  SubOctree<E> e000, e001, e010, e011, e100, e101, e110, e111;

  /**
   * Construct an empty Octree. The Octree starts with a single octree node and
   * grows as elements are inserted. Size of an octree must be at least 4 and it
   * must be a power of 2.
   * 
   * @param size
   *          Size of each dimension of the octree (octree can store up to
   *          size*size*size elements).
   */
  public Octree(int size) {
    // First check the size
    if (size < 4) {
      throw new IllegalArgumentException(String.format(
          "size must be >= 4 (%d given)", size));
    }
    if (!OctreeUtil.checkPowerOfTwo(size)) {
      throw new IllegalArgumentException(String.format(
          "size must be a power of 2 (%d given)", size));
    }
    this.size = size;

    this.x1 = 0;
    this.y1 = 0;
    this.z1 = 0;
  }

  Octree(int x1, int y1, int z1, int size) {
    assert size >= 4;
    assert OctreeUtil.checkPowerOfTwo(size);
    this.size = size;

    this.x1 = x1;
    this.y1 = y1;
    this.z1 = z1;
  }

  @Override
  public E get(int x, int y, int z) {
    boolean bx = OctreeUtil.checkAndWhichHalf(x, x1, size);
    boolean by = OctreeUtil.checkAndWhichHalf(y, y1, size);
    boolean bz = OctreeUtil.checkAndWhichHalf(z, z1, size);

    if (bx) {
      if (by) {
        if (bz) {
          if (e000 != null) return e000.get(x, y, z);
          else return null;
        } else {
          if (e001 != null) return e001.get(x, y, z);
          else return null;
        }
      } else {
        if (bz) {
          if (e010 != null) return e010.get(x, y, z);
          else return null;
        } else {
          if (e011 != null) return e011.get(x, y, z);
          else return null;
        }
      }
    } else {
      if (by) {
        if (bz) {
          if (e100 != null) return e100.get(x, y, z);
          else return null;
        } else {
          if (e101 != null) return e101.get(x, y, z);
          else return null;
        }
      } else {
        if (bz) {
          if (e110 != null) return e110.get(x, y, z);
          else return null;
        } else {
          if (e111 != null) return e111.get(x, y, z);
          else return null;
        }
      }
    }
  }

  @Override
  public void set(int x, int y, int z, E object) {
    boolean bx = OctreeUtil.checkAndWhichHalf(x, x1, size);
    boolean by = OctreeUtil.checkAndWhichHalf(y, y1, size);
    boolean bz = OctreeUtil.checkAndWhichHalf(z, z1, size);
    final int nsz = size / 2;

    if (bx) {
      if (by) {
        if (bz) {
          if (e000 == null) {
            if (size == 4) e000 = new LastOctree<E>(x1, y1, z1);
            else e000 = new Octree<E>(x1, y1, z1, nsz);
          }
          e000.set(x, y, z, object);
        } else {
          if (e001 == null) {
            if (size == 4) e001 = new LastOctree<E>(x1, y1, z1 + 2);
            else e001 = new Octree<E>(x1, y1, z1 + nsz, nsz);
          }
          e001.set(x, y, z, object);
        }
      } else {
        if (bz) {
          if (e010 == null) {
            if (size == 4) e010 = new LastOctree<E>(x1, y1 + 2, z1);
            else e010 = new Octree<E>(x1, y1 + nsz, z1, nsz);
          }
          e010.set(x, y, z, object);
        } else {
          if (e011 == null) {
            if (size == 4) e011 = new LastOctree<E>(x1, y1 + 2, z1 + 2);
            else e011 = new Octree<E>(x1, y1 + nsz, z1 + nsz, nsz);
          }
          e011.set(x, y, z, object);
        }
      }
    } else {
      if (by) {
        if (bz) {
          if (e100 == null) {
            if (size == 4) e100 = new LastOctree<E>(x1 + 2, y1, z1);
            else e100 = new Octree<E>(x1 + nsz, y1, z1, nsz);
          }
          e100.set(x, y, z, object);
        } else {
          if (e101 == null) {
            if (size == 4) e101 = new LastOctree<E>(x1 + 2, y1, z1 + 2);
            else e101 = new Octree<E>(x1 + nsz, y1, z1 + nsz, nsz);
          }
          e101.set(x, y, z, object);
        }
      } else {
        if (bz) {
          if (e110 == null) {
            if (size == 4) e110 = new LastOctree<E>(x1 + 2, y1 + 2, z1);
            else e110 = new Octree<E>(x1 + nsz, y1 + nsz, z1, nsz);
          }
          e110.set(x, y, z, object);
        } else {
          if (e111 == null) {
            if (size == 4) e111 = new LastOctree<E>(x1 + 2, y1 + 2, z1 + 2);
            else e111 = new Octree<E>(x1 + nsz, y1 + nsz, z1 + nsz, nsz);
          }
          e111.set(x, y, z, object);
        }
      }
    }
  }

  @Override
  public Iterator<OctreeElement<E>> iterator() {
    return new OctreeIterator<E>(this);
  }

  /**
   * Produces an iterator over suboctrees of size suboctree_size. The iterator
   * only covers the suboctrees that actually contain some data (or contained
   * some data that was overwritten with nulls).
   * 
   * @param suboctree_size
   *          Size of suboctrees that you wish to iterate over.
   * @return Iterator over Octrees.
   */
  public Iterator<Octree<E>> getSubIterator(int suboctree_size) {
    return new SubOctreeIterator<E>(this, suboctree_size);
  }
}
