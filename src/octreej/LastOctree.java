package octreej;

class LastOctree<E> implements SubOctree<E> {
  // An Octree box of size 2x2x2. "Left" corner is x1,y1,z1
  final int x1, y1, z1;
  E e000, e001, e010, e011, e100, e101, e110, e111;
  
  LastOctree(int x1, int y1, int z1) {
    this.x1 = x1; this.y1 = y1; this.z1 = z1;
  }

  @Override
  public void set(int x, int y, int z, E object) {
    boolean bx = OctreeUtil.checkAndWhichHalf(x, x1, 2);
    boolean by = OctreeUtil.checkAndWhichHalf(y, y1, 2);
    boolean bz = OctreeUtil.checkAndWhichHalf(z, z1, 2);
    
    if (bx) {
      if (by) {
        if (bz) e000 = object;
        else e001 = object;
      } else {
        if (bz) e010 = object;
        else e011 = object;
      }
    } else {
      if (by) {
        if (bz) e100 = object;
        else e101 = object;
      } else {
        if (bz) e110 = object;
        else e111 = object;
      }
    }
  }

  @Override
  public E get(int x, int y, int z) {
    boolean bx = OctreeUtil.checkAndWhichHalf(x, x1, 2);
    boolean by = OctreeUtil.checkAndWhichHalf(y, y1, 2);
    boolean bz = OctreeUtil.checkAndWhichHalf(z, z1, 2);
    
    if (bx) {
      if (by) {
        if (bz) return e000;
        else return e001;
      } else {
        if (bz) return e010;
        else return e011;
      }
    } else {
      if (by) {
        if (bz) return e100;
        else return e101;
      } else {
        if (bz) return e110;
        else return e111;
      }
    }
  }
}
