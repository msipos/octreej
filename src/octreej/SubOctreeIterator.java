package octreej;

import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.NoSuchElementException;

class SubOctreeIterator<E> implements Iterator<Octree<E>> {
  IteratorState state;
  final ArrayDeque<StackElement<E>> stack;
  Octree<E> current;
  int suboctree_size;

  SubOctreeIterator(Octree<E> tree, int suboctree_size) {
    state = IteratorState.GOING;
    stack = new ArrayDeque<StackElement<E>>();
    stack.push(new StackElement<E>(tree));

    this.suboctree_size = suboctree_size;
    if (suboctree_size < 4) {
      throw new IllegalArgumentException(String.format(
          "suboctree_size must be >= 4 (%d given)", suboctree_size));
    }
    if (!OctreeUtil.checkPowerOfTwo(suboctree_size)) {
      throw new IllegalArgumentException(String.format(
          "suboctree_size must be a power of 2 (%d given)", suboctree_size));
    }
    if (suboctree_size >= tree.getSize()) {
      throw new IllegalArgumentException(String.format(
          "suboctree_size must be < tree size (%d given)", suboctree_size));
    }

    goToNext();
  }

  private void goToNext() {
    if (state == IteratorState.FINISHED) return;

    search: for (;;) {
      if (stack.isEmpty()) {
        state = IteratorState.FINISHED;
        return;
      }

      StackElement<E> element = stack.getFirst();
      SubOctree<E> octree = element.octree;

      assert octree instanceof Octree<?>;
      Octree<E> octree2 = (Octree<E>) octree;

      if (octree2.getSize() == 2*suboctree_size) {
        // This node has suboctrees that we want

        switch (element.number) {
        case 0:
          element.number++;
          if (octree2.e000 != null) {
            current = (Octree<E>) octree2.e000;
            break search;
          }
        case 1:
          element.number++;
          if (octree2.e001 != null) {
            current = (Octree<E>) octree2.e001;
            break search;
          }
        case 2:
          element.number++;
          if (octree2.e010 != null) {
            current = (Octree<E>) octree2.e010;
            break search;
          }
        case 3:
          element.number++;
          if (octree2.e011 != null) {
            current = (Octree<E>) octree2.e011;
            break search;
          }
        case 4:
          element.number++;
          if (octree2.e100 != null) {
            current = (Octree<E>) octree2.e100;
            break search;
          }
        case 5:
          element.number++;
          if (octree2.e101 != null) {
            current = (Octree<E>) octree2.e101;
            break search;
          }
        case 6:
          element.number++;
          if (octree2.e110 != null) {
            current = (Octree<E>) octree2.e110;
            break search;
          }
        case 7:
          element.number++;
          if (octree2.e111 != null) {
            current = (Octree<E>) octree2.e111;
            break search;
          }
        case 8:
          stack.pop();
          continue search;
        }
      } else {
        // This node is too large, iterate further down the hierarchy

        switch (element.number) {
        case 0:
          element.number++;
          if (octree2.e000 != null) {
            stack.push(new StackElement<E>(octree2.e000));
            continue search;
          }
        case 1:
          element.number++;
          if (octree2.e001 != null) {
            stack.push(new StackElement<E>(octree2.e001));
            continue search;
          }
        case 2:
          element.number++;
          if (octree2.e010 != null) {
            stack.push(new StackElement<E>(octree2.e010));
            continue search;
          }
        case 3:
          element.number++;
          if (octree2.e011 != null) {
            stack.push(new StackElement<E>(octree2.e011));
            continue search;
          }
        case 4:
          element.number++;
          if (octree2.e100 != null) {
            stack.push(new StackElement<E>(octree2.e100));
            continue search;
          }
        case 5:
          element.number++;
          if (octree2.e101 != null) {
            stack.push(new StackElement<E>(octree2.e101));
            continue search;
          }
        case 6:
          element.number++;
          if (octree2.e110 != null) {
            stack.push(new StackElement<E>(octree2.e110));
            continue search;
          }
        case 7:
          element.number++;
          if (octree2.e111 != null) {
            stack.push(new StackElement<E>(octree2.e111));
            continue search;
          }
        case 8:
          stack.pop();
          continue search;
        }
      }
    }
  }

  @Override
  public boolean hasNext() {
    if (state == IteratorState.GOING) {
      return true;
    }
    return false;
  }

  @Override
  public Octree<E> next() {
    if (state == IteratorState.FINISHED) {
      throw new NoSuchElementException();
    }

    // Switch iterator_element1 <-> iterator_element2.
    Octree<E> rv = current;

    // goToNext modifies iterator_element
    goToNext();

    return rv;
  }

  @Override
  public void remove() {
    throw new UnsupportedOperationException();
  }
}
