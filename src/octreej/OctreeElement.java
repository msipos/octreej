package octreej;

/**
 * Helper object for iterating over an Octree. It allows the iterating loop to
 * access x, y and z indices of underlying elements. This object is reused
 * within the iterator, so it should not be saved.
 * 
 * @author Maksim Sipos
 * 
 * @param <E>
 *          Type of element within the octree.
 */
public class OctreeElement<E> {
  int x, y, z;

  public int getX() {
    return x;
  }

  public int getY() {
    return y;
  }

  public int getZ() {
    return z;
  }

  E element;

  /**
   * Element currently iterated over.
   */
  public E get() {
    return element;
  }
}
