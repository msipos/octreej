package octreej;

class OctreeUtil {
    // Returns true if first half otherwise second
  static boolean checkAndWhichHalf(int x, int x1, int size) {
    if (x < x1 || x >= (x1 + size)) {
      throw new ArrayIndexOutOfBoundsException(String.format(
          "invalid index %d (must be %d-%d)", x, x1, x1 + size - 1));
    }
    return (x - x1) < (size / 2);
  }
  
  static void checkIndex(int x, int x1, int size) {
    if (x < x1 || x >= (x1 + size)) {
      throw new ArrayIndexOutOfBoundsException(String.format(
          "invalid index %d (must be %d-%d)", x, x1, x1 + size - 1));
    }
  }
  
  static boolean checkPowerOfTwo(int x) {
    for(;;) {
      if (x == 1) return true;
      if (x % 2 == 1) return false;
      x = x / 2;
    }
  }
}
