package octreej;

import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.NoSuchElementException;

class OctreeIterator<E> implements Iterator<OctreeElement<E>> {
  IteratorState state;
  final ArrayDeque<StackElement<E>> stack;
  OctreeElement<E> iterator_element;
  OctreeElement<E> iterator_element2;

  OctreeIterator(Octree<E> tree) {
    state = IteratorState.GOING;
    stack = new ArrayDeque<StackElement<E>>();
    stack.push(new StackElement<E>(tree));
    iterator_element = new OctreeElement<E>();
    iterator_element2 = new OctreeElement<E>();

    goToNext();
  }

  private void goToNext() {
    if (state == IteratorState.FINISHED) return;

    search: for (;;) {
      if (stack.isEmpty()) {
        state = IteratorState.FINISHED;
        return;
      }

      StackElement<E> element = stack.getFirst();
      SubOctree<E> octree = element.octree;

      if (octree instanceof Octree<?>) {
        // ////////////////////////////////////////////////////////////////
        // We are in an Octree
        // ////////////////////////////////////////////////////////////////

        Octree<E> octree2 = (Octree<E>) octree;

        switch (element.number) {
        case 0:
          element.number++;
          if (octree2.e000 != null) {
            stack.push(new StackElement<E>(octree2.e000));
            continue search;
          }
        case 1:
          element.number++;
          if (octree2.e001 != null) {
            stack.push(new StackElement<E>(octree2.e001));
            continue search;
          }
        case 2:
          element.number++;
          if (octree2.e010 != null) {
            stack.push(new StackElement<E>(octree2.e010));
            continue search;
          }
        case 3:
          element.number++;
          if (octree2.e011 != null) {
            stack.push(new StackElement<E>(octree2.e011));
            continue search;
          }
        case 4:
          element.number++;
          if (octree2.e100 != null) {
            stack.push(new StackElement<E>(octree2.e100));
            continue search;
          }
        case 5:
          element.number++;
          if (octree2.e101 != null) {
            stack.push(new StackElement<E>(octree2.e101));
            continue search;
          }
        case 6:
          element.number++;
          if (octree2.e110 != null) {
            stack.push(new StackElement<E>(octree2.e110));
            continue search;
          }
        case 7:
          element.number++;
          if (octree2.e111 != null) {
            stack.push(new StackElement<E>(octree2.e111));
            continue search;
          }
        case 8:
          stack.pop();
          continue search;
        }
      } else {
        // ////////////////////////////////////////////////////////////////
        // We are in a LastOctree
        // ////////////////////////////////////////////////////////////////

        LastOctree<E> octree2 = (LastOctree<E>) octree;
        switch (element.number) {
        case 0:
          element.number++;
          if (octree2.e000 != null) {
            iterator_element.x = octree2.x1;
            iterator_element.y = octree2.y1;
            iterator_element.z = octree2.z1;
            iterator_element.element = octree2.e000;
            break search;
          }
        case 1:
          element.number++;
          if (octree2.e001 != null) {
            iterator_element.x = octree2.x1;
            iterator_element.y = octree2.y1;
            iterator_element.z = octree2.z1+1;
            iterator_element.element = octree2.e001;
            break search;
          }
        case 2:
          element.number++;
          if (octree2.e010 != null) {
            iterator_element.x = octree2.x1;
            iterator_element.y = octree2.y1+1;
            iterator_element.z = octree2.z1;
            iterator_element.element = octree2.e010;
            break search;
          }
        case 3:
          element.number++;
          if (octree2.e011 != null) {
            iterator_element.x = octree2.x1;
            iterator_element.y = octree2.y1+1;
            iterator_element.z = octree2.z1+1;
            iterator_element.element = octree2.e011;
            break search;
          }
        case 4:
          element.number++;
          if (octree2.e100 != null) {
            iterator_element.x = octree2.x1+1;
            iterator_element.y = octree2.y1;
            iterator_element.z = octree2.z1;
            iterator_element.element = octree2.e100;
            break search;
          }
        case 5:
          element.number++;
          if (octree2.e101 != null) {
            iterator_element.x = octree2.x1+1;
            iterator_element.y = octree2.y1;
            iterator_element.z = octree2.z1+1;
            iterator_element.element = octree2.e101;
            break search;
          }
        case 6:
          element.number++;
          if (octree2.e110 != null) {
            iterator_element.x = octree2.x1+1;
            iterator_element.y = octree2.y1+1;
            iterator_element.z = octree2.z1;
            iterator_element.element = octree2.e110;
            break search;
          }
        case 7:
          element.number++;
          if (octree2.e111 != null) {
            iterator_element.x = octree2.x1+1;
            iterator_element.y = octree2.y1+1;
            iterator_element.z = octree2.z1+1;
            iterator_element.element = octree2.e111;
            break search;
          }
        case 8:
          stack.pop();
          continue search;
        }
      }
    }
  }

  @Override
  public boolean hasNext() {
    if (state == IteratorState.GOING) {
      return true;
    }
    return false;
  }

  @Override
  public OctreeElement<E> next() {
    if (state == IteratorState.FINISHED) {
      throw new NoSuchElementException();
    }

    // Switch iterator_element1 <-> iterator_element2.
    OctreeElement<E> tmp = iterator_element;
    iterator_element = iterator_element2;
    iterator_element2 = tmp;

    // goToNext modifies iterator_element
    goToNext();

    return iterator_element2;
  }

  @Override
  public void remove() {
    throw new UnsupportedOperationException();
  }
}
