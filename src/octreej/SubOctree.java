package octreej;

interface SubOctree<E> {
  /**
   * Set the element to position (x,y,z).  If (x,y,z) already contains an element
   * then it is overwritten with the new value.
   */
  void set(int x, int y, int z, E element);

  
  /**
   * Gets the element at position (x,y,z).
   * @return Value at (x,y,z) or null if that location is empty.
   */
  E get(int x, int y, int z);
}
