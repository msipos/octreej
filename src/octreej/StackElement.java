package octreej;


class StackElement<E> {
  SubOctree<E> octree;
  int number;

  public StackElement(SubOctree<E> octree) {
    this.octree = octree;
    number = 0;
  }
}